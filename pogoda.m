%MATLAB R2020b
%name:pogoda
%author:maciej_pekala
%date: 14.12.2020
%version: v1.0
clear; clc;

%ustawienie czasu
t = timer('ExecutionMode','fixedSpacing', 'Period',1800);
t.TimerFcn  = @(~,~)odczyt;
start(t)

function odczyt

%odczytanie danych
station1_imgw_url = 'https://danepubliczne.imgw.pl/api/data/synop/station/katowice/format/json';
station2_imgw_url = 'https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala/format/json';
station3_imgw_url = 'https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json';
station1_wttr_url = 'http://wttr.in/katowice?format=j1';
station3_wttr_url = 'http://wttr.in/raciborz?format=j1';
station2_wttr_url = 'http://wttr.in/bielsko-biala?format=j1';

station1_imgw_url = webread(station1_imgw_url);
station2_imgw_url = webread(station2_imgw_url);
station3_imgw_url = webread(station3_imgw_url);
station1_wttr_url = webread(station1_wttr_url);
station2_wttr_url = webread(station2_wttr_url);
station3_wttr_url = webread(station3_wttr_url);


%odczyt temperatury
s_1_imgw_temp=station1_imgw_url.temperatura;
s_2_imgw_temp=station2_imgw_url.temperatura;
s_3_imgw_temp=station3_imgw_url.temperatura;
s_1_wttr_temp=station1_wttr_url.current_condition.temp_C;
s_2_wttr_temp=station2_wttr_url.current_condition.temp_C;
s_3_wttr_temp=station3_wttr_url.current_condition.temp_C;

s_1_imgw_temp= str2double(s_1_imgw_temp);
s_2_imgw_temp= str2double(s_2_imgw_temp);
s_3_imgw_temp= str2double(s_3_imgw_temp);

%odczyt cisnienia
s_1_imgw_cisnienie=station1_imgw_url.cisnienie;
s_2_imgw_cisnienie=station2_imgw_url.cisnienie;
s_3_imgw_cisnienie=station3_imgw_url.cisnienie;
s_1_wttr_cisnienie=station1_wttr_url.current_condition.pressure;
s_2_wttr_cisnienie=station2_wttr_url.current_condition.pressure;
s_3_wttr_cisnienie=station3_wttr_url.current_condition.pressure;

%odczyt predkosci wiatru
s_1_imgw_wiatr=station1_imgw_url.predkosc_wiatru;
s_2_imgw_wiatr=station2_imgw_url.predkosc_wiatru;
s_3_imgw_wiatr=station3_imgw_url.predkosc_wiatru;
s_1_wttr_wiatr=station1_wttr_url.current_condition.windspeedKmph;
s_2_wttr_wiatr=station2_wttr_url.current_condition.windspeedKmph;
s_3_wttr_wiatr=station3_wttr_url.current_condition.windspeedKmph;

s_1_imgw_wiatr=str2double(s_1_imgw_wiatr);
s_2_imgw_wiatr=str2double(s_2_imgw_wiatr);
s_3_imgw_wiatr=str2double(s_3_imgw_wiatr);

%odczyt wilgotności
s_1_imgw_wilgotnosc=station1_imgw_url.wilgotnosc_wzgledna;
s_2_imgw_wilgotnosc=station2_imgw_url.wilgotnosc_wzgledna;
s_3_imgw_wilgotnosc=station3_imgw_url.wilgotnosc_wzgledna;
s_1_wttr_wilgotnosc=station1_wttr_url.current_condition.humidity;
s_2_wttr_wilgotnosc=station2_wttr_url.current_condition.humidity;
s_3_wttr_wilgotnosc=station3_wttr_url.current_condition.humidity;

%odczyt czasu
s_1_imgw_data_czas=station1_imgw_url.data_pomiaru + " " +  station1_imgw_url.godzina_pomiaru;
s_2_imgw_data_czas=station2_imgw_url.data_pomiaru + " " +  station2_imgw_url.godzina_pomiaru;
s_3_imgw_data_czas=station3_imgw_url.data_pomiaru + " " +  station3_imgw_url.godzina_pomiaru;
s_1_wttr_data_czas=station1_wttr_url.current_condition.localObsDateTime ;
s_2_wttr_data_czas=station2_wttr_url.current_condition.localObsDateTime ;
s_3_wttr_data_czas=station3_wttr_url.current_condition.localObsDateTime ;

%wyliczenie temperatury odczuwalnej imgw
T_1_imgw= 13.12+0.6215*s_1_imgw_temp-11.37*(s_1_imgw_wiatr^0.16)+0.3965*s_1_imgw_temp*(s_1_imgw_wiatr^0.16);
T_2_imgw= 13.12+0.6215*s_2_imgw_temp-11.37*(s_2_imgw_wiatr^0.16)+0.3965*s_2_imgw_temp*(s_2_imgw_wiatr^0.16);
T_3_imgw= 13.12+0.6215*s_3_imgw_temp-11.37*(s_3_imgw_wiatr^0.16)+0.3965*s_3_imgw_temp*(s_3_imgw_wiatr^0.16);

%odczyt temperatury odczuwalnej wttr
T_1_wttr=station1_wttr_url.current_condition.FeelsLikeC;
T_2_wttr=station2_wttr_url.current_condition.FeelsLikeC;
T_3_wttr=station3_wttr_url.current_condition.FeelsLikeC;

%stworzenie pliku do nadpisywania
wyniki_pogoda= fopen("wynik_pogoda.csv", "w");
fprintf(wyniki_pogoda, "Czas;Temperatura;TemperaturaOdczuwalna;PredkoscWiatru;Cisnienie;Wilgotnosc\n");
fprintf(wyniki_pogoda, "%s;%.1f;%.1f;%.1f;%s;%s\n",s_1_imgw_data_czas,s_1_imgw_temp,T_1_imgw,s_1_imgw_wiatr,s_1_imgw_cisnienie,s_1_imgw_wilgotnosc);
fprintf(wyniki_pogoda, "%s;%.1f;%.1f;%.1f;%s;%s\n",s_2_imgw_data_czas,s_2_imgw_temp,T_2_imgw,s_2_imgw_wiatr,s_2_imgw_cisnienie,s_2_imgw_wilgotnosc);
fprintf(wyniki_pogoda, "%s;%.1f;%.1f;%.1f;%s;%s\n",s_3_imgw_data_czas,s_3_imgw_temp,T_3_imgw,s_3_imgw_wiatr,s_3_imgw_cisnienie,s_3_imgw_wilgotnosc);

fprintf(wyniki_pogoda, "%s;%s;%s;%s;%s;%s\n",s_1_wttr_data_czas,s_1_wttr_temp,T_1_wttr,s_1_wttr_wiatr,s_1_wttr_cisnienie,s_1_wttr_wilgotnosc);
fprintf(wyniki_pogoda, "%s;%s;%s;%s;%s;%s\n",s_2_wttr_data_czas,s_2_wttr_temp,T_2_wttr,s_2_wttr_wiatr,s_2_wttr_cisnienie,s_2_wttr_wilgotnosc);
fprintf(wyniki_pogoda, "%s;%s;%s;%s;%s;%s\n",s_3_wttr_data_czas,s_3_wttr_temp,T_3_wttr,s_3_wttr_wiatr,s_3_wttr_cisnienie,s_3_wttr_wilgotnosc);

znaki = readlines('wynik_pogoda.csv');
plik = fopen('wynik_pogoda.csv', 'w');
znaki = unique(znaki, 'stable');
for i = 1:length(znaki)
    fprintf(plik,"%s\n", znaki(i));
end


%pokazanie danych na konsoli
disp(s_1_imgw_data_czas)
disp(s_1_imgw_temp)
disp(s_2_imgw_data_czas)
disp(s_2_imgw_temp)
disp(s_3_imgw_data_czas)
disp(s_3_imgw_temp)
disp(s_1_wttr_data_czas)
disp(s_1_wttr_temp)
disp(s_2_wttr_data_czas)
disp(s_2_wttr_temp)
disp(s_3_wttr_data_czas)
disp(s_3_wttr_temp)



fclose('all');
end 